const express = require('express');

const router = express.Router();

const reservaController = require('../controllers/reserva');

router.post('/reservar',reservaController.reserva_create_post);

module.exports = router;