const express = require('express');
const router = express.Router();
const usuarioController = require('../controllers/usuarios');

router.post('/create', usuarioController.usuario_create_post);

module.exports = router;