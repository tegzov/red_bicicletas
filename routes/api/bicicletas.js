const express = require('express');

const router = express.Router();

const bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

//Rutas API
router.get('/', bicicletaController.bicicleta_list);
router.post('/create', bicicletaController.bicicletas_create);
router.delete('/:id/delete', bicicletaController.bicicletas_delete);
router.put('/:id/update', bicicletaController.bicicletas_update);

module.exports = router;