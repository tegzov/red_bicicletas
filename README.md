# Red Bicicletas :bike:

Empezando proyecto "Red Bicicletas" del curso "Desarrollo del lado servidor: NodeJS, Express y MongoDB".

## ¡Solo se puede avanzar! :rocket:

¡Empezando por el CRUD de datos en memoria hasta tener una app con MongoDB como Base de Datos!

## ¿Sos del peer-review? ¡No te contengas, quiero aprender! :wink:

Cualquier critica es bienvenida, todo es para mejorar.

## Power ups :zap:

El manjar de casi todos los programadores :coffee: