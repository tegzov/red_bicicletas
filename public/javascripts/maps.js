var mymap = L.map('main_map').setView([-26.177360,-58.170890], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 24,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoidGVnem92IiwiYSI6ImNrZXczY2ZhODA1dzYydWszZDg3eTZjZzQifQ.MAwl1m4saVXNdVmvfG_Y8g',
}).addTo(mymap);

/*L.marker([-26.181323799999998,-58.1779131]).addTo(mymap);
L.marker([-26.177370,-58.170890]).addTo(mymap);
L.marker([-26.185368799973652,-58.17482319521485]).addTo(mymap);*/

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        result.bicicletas.forEach( function(bici) {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
        });
    }
});