let mongoose = require('mongoose');
let Schema = mongoose.Schema;
const moment = require('moment');

const reservaSchema = new Schema({
    code: Number,
    desde: Date,
    hasta: Date,
    bicicleta: { type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta' },
    usuario: {type: mongoose.Schema.Types.ObjectId, ref: 'Usuario'}
});

reservaSchema.methods.diasDeReserva = function(){

    return moment(this.hasta).diff(moment(this.desde), 'days') + 1;

};

const Reserva = mongoose.model('Reserva', reservaSchema);

module.exports = Reserva;