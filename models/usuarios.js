let mongoose = require('mongoose');
let Schema = mongoose.Schema;
const Reserva = require('./reserva');

let usuarioSchema = new Schema({
    code: Number,
    nombre: String
});

usuarioSchema.methods.reservar = function(biciCode, desde, hasta){

    const newReserva = new Reserva({ usuario: this._id, bicicleta: biciCode, desde: desde, hasta: hasta });

    return Reserva.create(newReserva);
};

const Usuario = mongoose.model('Usuario', usuarioSchema);

module.exports = Usuario;