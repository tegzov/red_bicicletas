let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true }
    }
});

bicicletaSchema.statics.populateBici = function(code, color, modelo, lat, lon){

    const newBici = {
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: [lat,lon]
    };

    return newBici;
};

bicicletaSchema.statics.addBici = function(nuevaBici){
    this.create(nuevaBici);
};

bicicletaSchema.statics.showAllBicis = function(){
    return this.find({});
};

bicicletaSchema.statics.findByCode = function(aCode){
    return this.findOne({code: aCode});
};

bicicletaSchema.statics.updateBici = function(biciCode, modColor, modModelo, modLat, modLon){
    //Parametro {new: true} para devolver el valor del nuevo documento
    return this.findOneAndUpdate({code: biciCode}, { color: modColor, modelo: modModelo, ubicacion: [modLat, modLon]}, {new: true});
};

bicicletaSchema.statics.removeByCode = function(aCode){
    return this.deleteOne({code: aCode});
};

const Bicicleta = mongoose.model('Bicicleta', bicicletaSchema);

module.exports = Bicicleta;