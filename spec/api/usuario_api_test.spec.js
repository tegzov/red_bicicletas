const Bicicleta = require('../../models/bicicletas');
const Reserva = require('../../models/reserva');
const request = require('request');
const rp = require('request-promise');
const server = require('../../bin/www');
const Usuario = require('../../models/usuarios');

describe('Bicicleta API', () => {

    afterEach(async () => {
        await Bicicleta.deleteMany({});
        await Reserva.deleteMany({});
        await Usuario.deleteMany({});
    });

    it('Status 200', async () => { 

        const getRequest = async() => {
            const urlGet = 'http://localhost:3000/api/usuarios/';
            const options = {
                method: 'GET',
                json: true,
                uri: urlGet,
                body: {},
                resolveWithFullResponse: true
            };
            const response = await rp(options);
            return response;
        };

        const result = await getRequest();
        const {body:{usuarios}} = result; 
        expect(result.statusCode).toBe(200);
        expect(usuarios.length).toBe(0); 
    });

    describe('CREATE BICICLETAS /', () => {
        it('Status 200', async () => {
            const postRequest = async(newUsr) => {
                const urlPost = 'http://localhost:3000/api/usuarios/create';
                const headers = {'Content-Type': 'application/json'};
                const options = {
                    method: 'POST',
                    headers: headers,
                    json: true,
                    uri: urlPost,
                    body: newUsr,
                    resolveWithFullResponse: true
                };

                const response = await rp(options);
                return response;
            }
            const newUsr = {nombre: "Tomas Gonzale Oviedo"};
            const result = await postRequest(newUsr);
            const { body } = result; 
            expect(result.statusCode).toBe(200);
            expect(body.nombre).toBe(newUsr.nombre);
        });
    });

    describe('Usuario reserva una bici', () => {
        it('debe existir una reserva', async() => {
            const newUsuario = new Usuario({
                code: 1, 
                nombre: "Tomas"
            });
            newUsuario.save();
            const newBicicleta = new Bicicleta({
                code: 1,
                color: "azul marino",
                modelo: "playera"
            });
            newBicicleta.save();

            const hoy = new Date();
            const mañana = new Date();

            mañana.setDate(hoy.getDate() + 1);

            const postRequest = async(newReserva) => {
                const urlPost = 'http://localhost:3000/api/usuarios/reservar';
                const headers = {'Content-Type': 'application/json'};
                const options = {
                    method: 'POST',
                    headers: headers,
                    json: true,
                    uri: urlPost,
                    body: newReserva,
                    resolveWithFullResponse: true
                };

                const response = await rp(options);
                return response;
            }
            const newReserva = {id: newUsuario.id, bici_id: newBicicleta.id, hoy, mañana};

            const result = await postRequest(newReserva);
            const { body } = result; 
            expect(result.statusCode).toBe(200);
        });
    });

});