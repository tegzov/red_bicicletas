const Bicicleta = require('../../models/bicicletas');
const request = require('request');
const rp = require('request-promise');
const server = require('../../bin/www');
const { deleteOne } = require('../../models/bicicletas');

describe('Bicicleta API', () => {

    afterEach(async () => {
        await Bicicleta.deleteMany({});
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', async () => { 

            const getRequest = async() => {
                const urlGet = 'http://localhost:3000/api/bicicletas';
                const options = {
                    method: 'GET',
                    json: true,
                    uri: urlGet,
                    body: {},
                    resolveWithFullResponse: true
                };
                const response = await rp(options);
                return response;
            };

            const result = await getRequest();
            const {body:{bicicletas}} = result; 
            expect(result.statusCode).toBe(200);
            expect(bicicletas.length).toBe(0); 
        });
    });

    describe('CREATE BICICLETAS /', () => {
        it('Status 200', async () => {
            const postRequest = async(newBici) => {
                const urlPost = 'http://localhost:3000/api/bicicletas/create';
                const headers = {'Content-Type': 'application/json'};
                const options = {
                    method: 'POST',
                    headers: headers,
                    json: true,
                    uri: urlPost,
                    body: newBici,
                    resolveWithFullResponse: true
                };

                const response = await rp(options);
                return response;
            }
            const newBici = {id: 3, color: "marron", modelo: "mountain bike", lat: 70, lon: 80};
            const result = await postRequest(newBici);
            const { body:{bicicleta} } = result; 
            expect(result.statusCode).toBe(200);
            expect(bicicleta.code).toBe(newBici.id);
            expect(bicicleta.color).toBe(newBici.color);
            expect(bicicleta.modelo).toBe(newBici.modelo);
            expect(bicicleta.ubicacion).toEqual([newBici.lat,newBici.lon]);
        });
    });

    describe('UPDATE BICICLETAS /', () => {
        it('Status 200', async() => {

            const postRequest = async(newBici) => {
                const urlPost = 'http://localhost:3000/api/bicicletas/create';
                const headers = {'Content-Type': 'application/json'};
                const options = {
                    method: 'POST',
                    headers: headers,
                    json: true,
                    uri: urlPost,
                    body: newBici,
                    resolveWithFullResponse: true
                };

                const response = await rp(options);
                return response;
            }
            const newBici = {id: 3, color: "marron", modelo: "mountain bike", lat: 70, lon: 80};
            const result = await postRequest(newBici);
            expect(result.statusCode).toBe(200);

            const updateRequest = async(updateBici) => {
                const urlUpdate = 'http://localhost:3000/api/bicicletas/3/update';
                const headers = {'Content-Type': 'application/json'};
                const options = {
                    method: 'PUT',
                    headers: headers,
                    json: true,
                    uri: urlUpdate,
                    body: updateBici,
                    resolveWithFullResponse: true
                };

                const response = await rp(options);
                return response;
            };
            const updateBici = {color: "azul marino", modelo: "bici de la ciudad", lat: 10, lon: 20};
            const resultU = await updateRequest(updateBici);
            const { body:{bicicletaActualizada} } = resultU;

            expect(resultU.statusCode).toBe(200);
            expect(bicicletaActualizada.code).toBe(newBici.id);
            expect(bicicletaActualizada.color).toBe(updateBici.color);
            expect(bicicletaActualizada.modelo).toBe(updateBici.modelo);
            expect(bicicletaActualizada.ubicacion).toEqual([updateBici.lat,updateBici.lon]);
        });
    });

    describe('DELETE BICICLETAS /', () => {
        it('Status 200', async() => {

            const postRequest = async(newBici) => {
                const urlPost = 'http://localhost:3000/api/bicicletas/create';
                const headers = {'Content-Type': 'application/json'};
                const options = {
                    method: 'POST',
                    headers: headers,
                    json: true,
                    uri: urlPost,
                    body: newBici,
                    resolveWithFullResponse: true
                };

                const response = await rp(options);
                return response;
            }
            const newBici = {id: 3, color: "marron", modelo: "mountain bike", lat: 70, lon: 80};
            const result = await postRequest(newBici);
            expect(result.statusCode).toBe(200);

            const getRequest = async() => {
                const urlGet = 'http://localhost:3000/api/bicicletas';
                const options = {
                    method: 'GET',
                    json: true,
                    uri: urlGet,
                    body: {},
                    resolveWithFullResponse: true
                };
                const response = await rp(options);
                return response;
            };

            const resultG = await getRequest();
            const {body:{bicicletas}} = resultG;
            expect(resultG.statusCode).toBe(200);
            expect(bicicletas.length).toBe(1);

            const deleteRequest = async() => {
                const urlDelete = 'http://localhost:3000/api/bicicletas/3/delete';
                const headers = {'Content-Type': 'application/json'};
                const options = {
                    method: 'DELETE',
                    headers: headers,
                    json: true,
                    uri: urlDelete,
                    body: {},
                    resolveWithFullResponse: true
                };

                const response = await rp(options);
                return response;
            };
            const resultD = await deleteRequest();
            expect(resultD.statusCode).toBe(200);
            const resultG2 = await getRequest();
            expect(resultG2.statusCode).toBe(200);
            expect(resultG2.body.bicicletas.length).toBe(0);
        });
    });
});