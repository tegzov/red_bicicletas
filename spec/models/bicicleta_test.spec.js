let mongoose = require('mongoose');
let Bicicleta = require('../../models/bicicletas');

describe('Testing Bicicletas', function(){
    const cambiarTimer = (newTime) => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = newTime
    };
    beforeEach(function(done) {
        //cambiarTimer(10000);
        let mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('Estamos conectados a la Base de Datos');
            done();
        });  
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
            mongoose.disconnect();
        });
    });

    describe('Mostrar listado de bicis', () => {
        it('Bicicleta.showAllBicis', (done) => {
            Bicicleta.showAllBicis(function(err, listadoBicis){
                if(err) console.log(err);
                console.log('Listado de bicis...');
                expect(listadoBicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Cargar los datos de una bici', () => {
        it('Bicicleta.populateBici', (done) => {
            const timeOut = (newBici) => {
                setTimeout(function(){
                    return newBici;
                }, 100);
            };
            const nuevaBici = Bicicleta.populateBici(1,"verde","guerrera",15,16,timeOut);
            console.log('Se ha rellenado la nueva Bici');
            expect(nuevaBici.code).toBe(1);
            expect(nuevaBici.color).toBe("verde");
            expect(nuevaBici.modelo).toBe("guerrera");
            expect(nuevaBici.ubicacion).toEqual([15,16]);
            done();
        });
    });

    describe('Persistir la data en BD', () => {
        it('Bicicleta.addBici', (done) => {
            const timeOut = (newBici) => {
                setTimeout(function(){
                    return newBici;
                }, 100)
            }
            const nuevaBici = Bicicleta.populateBici(1,"verde","guerrera",15,16,timeOut);
            Bicicleta.addBici(nuevaBici,function(err){
                if(err) console.log(err);
                Bicicleta.showAllBicis(function(error, listadoBicis){
                    if(error) console.log(error);
                    expect(listadoBicis.length).toEqual(1);
                    expect(listadoBicis[0].code).toEqual(nuevaBici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            Bicicleta.showAllBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
            
                const timeOut = (newBici) => {
                    setTimeout(function(){
                        return newBici;
                    }, 100);
                };
                const nuevaBici = Bicicleta.populateBici(1,"verde","guerrera",15,16,timeOut);
                Bicicleta.addBici(nuevaBici, function(err){
                    if(err) console.log(err);
                    const nuevaBici2 = Bicicleta.populateBici(2,"amarilla","playera",70,80,timeOut);
                    Bicicleta.addBici(nuevaBici2,function(err){
                        if(err) console.log(err);
                        Bicicleta.findByCode(nuevaBici.code ,function(error, targetBici){
                            if(error) console.log(error);
                            console.log('Estamos buscando la bici...', nuevaBici);
                            expect(targetBici.code).toEqual(nuevaBici.code);
                            expect(targetBici.color).toEqual(nuevaBici.color);
                            expect(targetBici.modelo).toEqual(nuevaBici.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });
});