const Bicicleta = require('../../models/bicicletas');
const Reserva = require('../../models/reserva');
const Usuario = require('../../models/usuarios');
const mongoose = require('mongoose');
const rp = require('request-promise');

describe('Bicicleta API', () => {

    beforeAll(async () => {
        const mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true});
        mongoose.Promise = global.Promise;
        //Para hacer updates
        mongoose.set('useFindAndModify', false);
        const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', function(){
                console.log('Estamos conectados a la DB');
            });
        });

    afterEach(async () => {
        await Bicicleta.deleteMany({});
        await Reserva.deleteMany({});
        await Usuario.deleteMany({});
    });

    describe('Usuario reserva una bici', () => {
        it('debe existir una reserva', async() => {
            const newUsuario = new Usuario({
                code: 1, 
                nombre: "Tomas"
            });
            newUsuario.save();
            const newBicicleta = new Bicicleta({
                code: 1,
                color: "azul marino",
                modelo: "playera"
            });
            newBicicleta.save();

            const hoy = new Date();
            const mañana = new Date();

            mañana.setDate(hoy.getDate() + 1);
            const newReserva = await newUsuario.reservar(newBicicleta.id, hoy, mañana);
            const reservaData = await Reserva.find({}).populate('bicicleta').populate('usuario');
            expect(reservaData.length).toBe(1);
            expect(reservaData[0].diasDeReserva()).toBe(2);
            expect(reservaData[0].bicicleta.code).toBe(1);
            expect(reservaData[0].usuario.nombre).toBe(newUsuario.nombre);
        });
    });

});