const Usuario = require("../../models/usuarios");

exports.usuarios_list = async function (req, res){
    const usuarioLista = await Usuario.find({});
    res.status(200).json({
        usuarios: usuarioLista
    });
};

exports.usuarios_create = async function (req, res){
    const newUsuario = new Usuario({
        nombre: req.body.nombre
    });

    const guardarUsuario = await newUsuario.save();

    res.status(200).json(guardarUsuario);
};

exports.usuario_reserva = async function (req, res){
    const getUsuario = await Usuario.findById(req.body.id);
    await getUsuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta);
    res.status(200).send('Reserva realizada con exito');

};