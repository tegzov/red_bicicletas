let Bicicleta = require('../../models/bicicletas');

exports.bicicleta_list = async(req, res) => {
    try{
        const allBicis = await Bicicleta.showAllBicis();
        res.status(200).json({bicicletas: allBicis});
    }catch(error){
        res.status(404).send('Hubo un error en su solicitud');
    }
};

exports.bicicletas_create  = async (req, res) => {
    try{
        const bici = await Bicicleta.populateBici(req.body.id, req.body.color, req.body.modelo, req.body.lat, req.body.lon);

        Bicicleta.addBici(bici);
        
        res.status(200).json({
            bicicleta: bici
        });
    }catch(error){
        res.status(404).send('Hubo un error en su solicitud :(.');
    }
    
};

const bicicletas_findByCode = async(biciCode) => {

    const buscarBiciCode = await Bicicleta.findByCode(biciCode);

    const biciExiste = buscarBiciCode === null ? false  : true;

    return biciExiste;

};

exports.bicicletas_update = async (req,res) => {

    const biciCode = req.params.id;

    const biciExiste = await bicicletas_findByCode(biciCode);

    if(!biciExiste){
        return res.status(200).send(`La bici que desea borrar con codigo no existe o no fue encontrada :(`);
    }
    
    try{
        const updateBici = await Bicicleta.updateBici(biciCode, req.body.color, req.body.modelo, req.body.lat, req.body.lon);
        res.status(200).json({
            mensaje: `Bici actualizada con exito!`,
            bicicletaActualizada: updateBici});
    }catch(error){
        res.status(404).send(`Hubo un problema al realizar su solicitud :() ${error}`);
    }
    
};

exports.bicicletas_delete = async(req, res) => {

    const biciCode = req.params.id;

    const biciExiste = await bicicletas_findByCode(biciCode);

    if(!biciExiste){
        return res.status(200).send(`La bici que desea borrar con codigo no existe o no fue encontrada :(`);
    }

    try {

        await Bicicleta.removeByCode(biciCode);

        res.status(200).send('La bicicleta solicitada ha sido eliminada con exito!');

    }catch(error){
        res.status(404).send('Hubo un error en la solicitud :(');
    }
};