let Bicicleta = require('../models/bicicletas');

exports.bicicleta_list = function(req, res){
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
};

exports.bicicleta_create_get = function(req,res){
    res.render('bicicletas/create')
};

exports.bicicleta_create_post = function(req,res){
    let bici = new Bicicleta(req.body.idBici, req.body.colorBici, req.body.modeloBici);
    bici.ubicacion = [req.body.latBici, req.body.lonBici];
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
};

exports.bicicleta_delete_post = function(req,res){
    console.log(req.body.idBici);
    Bicicleta.removeById(req.body.idBici);
    res.redirect('/bicicletas');
};

exports.bicicleta_update_get = function(req,res){
    console.log('gets',req.params.id);
    let bici = Bicicleta.findById(Number(req.params.id));
    res.render('bicicletas/update', {bici});
};

exports.bicicleta_update_post = function(req,res){
    let bici = Bicicleta.findById(Number(req.body.idBici));
    bici.id = req.body.idBici;
    bici.color = req.body.colorBici;
    bici.modelo = req.body.modeloBici;
    bici.ubicacion = [req.body.latBici, req.body.lonBici];
    res.redirect('/bicicletas');
};