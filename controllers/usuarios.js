const Usuario = require('../models/usuarios');
const Reserva = require('../models/reserva');

exports.usuario_create_post = async (req, res) => {

    try{

        if(req.body.code  === undefined){
            throw 'El codigo esta vacio';
        }else if(req.body.nombre  === undefined){
            throw 'El nombre esta vacio';
        };

        const newUsuario = new Usuario({
            code: req.body.code, 
            nombre: req.body.nombre
        });

        res.status(200).json(newUsuario);

        return newUsuario;

    }catch(error){

        res.status(404).send('Hubo un error en su solicitud :( ' + error);

    };
};